<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Atomic Project</title>
    <link rel="stylesheet" href="../../src/BITM/SEIP_157156/Stylesheet/style.css">
</head>
<body>
    <div class="main-content">
        <div class="head">
            <h1>ATOMIC PROJECT</h1>
        </div>
        <div class="site_body">
            <div class="all-item">
                <a href="BookTitle/create.php">
                    <div class="book-title project-block">
                        <img src="../../resource/images/booktitle.png" alt="">
                        <br>
                        <h2>Book Title</h2>
                    </div>
                </a>
                <a href="BirthDay/create.php">
                    <div class="Birth-day project-block">
                        <img src="../../resource/images/birthday.png" alt="">
                        <br>
                        <h2>Birth Day</h2>
                    </div>
                </a>
                <a href="City/create.php">
                    <div class="city project-block">
                        <img src="../../resource/images/city.png" alt="">
                        <br>
                        <h2>City</h2>
                    </div>
                </a>
                <a href="Email/create.php">
                    <div class="email project-block">
                        <img src="../../resource/images/email.png" alt="">
                        <br>
                        <h2>E-mail</h2>
                    </div>
                </a>
                <a href="Gender/create.php">
                        <div class="gender project-block">
                        <img src="../../resource/images/gender.png" alt="">
                        <br>
                        <h2>Gender</h2>
                    </div>
                </a>
                <a href="Hobbies/create.php">
                        <div class="hobbies project-block">
                        <img src="../../resource/images/hobbies.png" alt="">
                        <br>
                        <h2>Hobbies</h2>
                    </div>
                </a>
                <a href="ProfilePicture/create.php">
                    <div class="profile-picture project-block">
                        <img src="../../resource/images/profilepicture.png" alt="">
                        <br>
                        <h2>Profile Picture</h2>
                    </div>
                </a>
                <a href="SummaryOfOrganization/create.php">
                    <div class="summary-of-organization project-block">
                        <img src="../../resource/images/summaryoforganization.png" alt="">
                        <br>
                        <h2>Summary Of Organization</h2>
                    </div>
                </a>
            </div>
            <div class="description">
                <div class="Side">
                    <p>Student Name:</p>
                    <p>SEIP:</p>
                    <p>Course Name:</p>
                    <p>Batch No:</p>
                    <p>Institute:</p>
                </div>
                <div class="Side"></div>
                <p>Ashok Barua.</p>
                <p>157156.</p>
                <p>Web App Development PHP.</p>
                <p>45.</p>
                <p>BITM.</p>
            </div>
        </div>
        <div class="footer">

        </div>
    </div>
</body>
</html>